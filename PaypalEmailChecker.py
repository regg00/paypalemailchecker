import csv
import logging
import sys
import time
from random import random

import click
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys


# Click CLI app definition
@click.command(context_settings=dict(help_option_names=["-h", "--help", "/h", "/?"]))
@click.option(
    "--path",
    prompt="Entrer le nom du fichier (e.x. emails.txt)",
    help="The relative path of the file containing emails to check. One email per line. Default to emails.txt",
    default="emails.txt",
)
@click.option(
    "--timer",
    help="Time to wait for the page to load for each checks. Default to 1 second.",
    default=1,
)
@click.option(
    "--chromedriver",
    help="Location of the chromedriver binaries. Default to current directory.",
    default="./chromedriver.exe",
)
@click.option(
    "--baseurl",
    help="Start URL to begin the tests.",
    default="https://www.paypal.com/cgi-bin/webscr?useraction=commit&cmd=_express-checkout&token=EC-21W7040756503764S",
)
@click.option(
    "--outfile",
    help="Where to save the logs/results.",
    default="output.log",
)
def start(path, timer, chromedriver, baseurl, outfile):
    """This app will validate if a Paypal account exist for each emails in an input file (e.g. emails.txt). Results are automatically saved in results.txt in the current folder."""

    valid_counter = 0
    invalid_counter = 0
    blocked_counter = 0

    # Configure logging to console and file
    logging.basicConfig(
        format="%(asctime)s, %(levelname)s, %(message)s",
        level=logging.INFO,
        handlers=[logging.FileHandler(outfile), logging.StreamHandler()],
    )
    logger = logging.getLogger("log")

    # Open the input file which contains the list of emails to validate
    try:
        f = open(path, "r")
    except:
        logger.error(f"File {path} does not exist.")
        sys.exit(1)

    click.echo(f"Running the validations. Exporting results to {outfile}.")

    # Start the chromedriver
    options = Options()
    options.add_argument("--window-size=640,480")
    chromedriver_location = chromedriver
    driver = webdriver.Chrome(chromedriver_location, options=options)

    for email in f:
        # Cleanup the email address, fill the form and press ENTER
        email = email.strip()
        driver.get(baseurl)
        elem = driver.find_element_by_id("email")
        elem.send_keys(email)
        elem.send_keys(Keys.RETURN)

        # Words or phrases to look out for to determine if the check is valid, blocked or invalid
        matches_for_invalid = [
            "Create an account to get Buyer Protection, Refunded Returns service, and more",
            "Ouvrez un compte pour bénéficier de la Protection des Achats, du service Frais de retour remboursés et plus encore.",
        ]
        matches_for_blocked = ["Challenge"]

        # Dumb sleep to wait for the page to load completely.
        time.sleep(timer)

        # Check in the source code of the page for the string defined above. If none are detected, this means that we are still on the logon screen and the account most probably exist
        # Invalid emails section
        if any(x in driver.page_source for x in matches_for_invalid):
            logging.warning(f"{email},invalid")
            invalid_counter += 1
        # Blocked emails section
        elif any(x in driver.page_source for x in matches_for_blocked):
            logging.error(f"{email},blocked")
            blocked_counter += 1
        # Valid emails section
        else:
            logging.info(f"{email},valid")
            valid_counter += 1

    # Close the browser window
    driver.close()
    # Dumb input so that the console stay open
    input(
        f"[*] Program completed. {valid_counter} emails are valid! See {outfile} for more details."
    )


if __name__ == "__main__":
    # Show the ASCII art on app start
    click.echo(
        "\n"
        "███╗   ██╗███████╗ █████╗ ██████╗\n"
        "████╗  ██║██╔════╝██╔══██╗██╔══██╗\n"
        "██╔██╗ ██║█████╗  ███████║██████╔╝\n"
        "██║╚██╗██║██╔══╝  ██╔══██║██╔══██╗\n"
        "██║ ╚████║███████╗██║  ██║██║  ██║\n"
        "╚═╝  ╚═══╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝\n"
    )
    # Start the actual app
    start()
