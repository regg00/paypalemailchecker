PaypalEmailChecker: Validate if an email has a valid Paypal account
=======================================

What is PaypalEmailChecker?
-------------

PaypalEmailChecker is using Selenium and ChromeDriver to open a Paypal login screen, type an email and then check the response to see if the email is a valid one in Paypal.
It takes a simple .txt file containing every emails to check as input and then output the results in a file called **results.txt** in the current directory.


```
███╗   ██╗███████╗ █████╗ ██████╗
████╗  ██║██╔════╝██╔══██╗██╔══██╗
██╔██╗ ██║█████╗  ███████║██████╔╝
██║╚██╗██║██╔══╝  ██╔══██║██╔══██╗
██║ ╚████║███████╗██║  ██║██║  ██║
╚═╝  ╚═══╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝

Usage: app.py [OPTIONS]

  This app will validate if a Paypal account exist for each emails in an
  input file (e.g. emails.txt). Results are automatically saved in
  results.txt in the current folder.

Options:
  --path TEXT          The relative path of the file containing emails to
                       check. One email per line. Default to emails.txt

  --timer INTEGER      Time to wait for the page to load for each checks.
                       Default to 1 second.

  --chromedriver TEXT  Location of the chromedriver binaries. Default to
                       current directory.

  --baseurl TEXT       Start URL to begin the tests.
  -h, /?, /h, --help   Show this message and exit.
```


Requirements
------------

You need Python 3.5 or later.

See **requirements.txt** for a list of required packages.


Quick start
-----------

PaypalEmailChecker can be run using the pre-compiled package:

    $ ./app.py --help

If you want to run from source code, install python and it's requirements:

    $ pip install -r requirements.xt
    $ python app.py --help

License
-------

PaypalEmailChecker is licensed under the terms of the MIT License (see the file
LICENSE).